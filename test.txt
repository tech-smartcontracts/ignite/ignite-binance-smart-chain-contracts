
// File: contracts/CidChain.sol

pragma solidity ^0.5.0;

library SafeMath {
    /**
    * @dev Subtracts two unsigned integers, reverts on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a);
        uint256 c = a - b;

        return c;
    }

    /**
    * @dev Adds two signed integers, reverts on overflow.
    */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require((b >= 0 && c >= a) || (b < 0 && c < a));

        return c;
    }

    /**
    * @dev Multiplies two unsigned integers, reverts on overflow.
    */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b);

        return c;
    }

    /**
    * @dev Integer division of two unsigned integers truncating the quotient, reverts on division by zero.
    */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b > 0);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    function max(uint256 a, uint256 b)
        internal
        pure
        returns (uint256)
    {
        return a >= b ? a : b;
    }
}

contract CidChain {
	using SafeMath for uint256;

	address public validator;
	uint256 public lastPushedBlock;

	mapping(address => bool) public allowList;
	mapping(uint256 => CidChainBlock) public cidChain;

	struct CidChainBlock {
		string btfsCid;
		address node;
		uint256 createdAt;
	}

	event ToAllowListAddressAdded(address node, uint256 addedAt);
	event InAllowListAddressRemoved(address node, uint256 removedAt);
	event CidChainBlockPushed(string btfsCid, address node);

	constructor() public {
		lastPushedBlock = 0;
		validator = msg.sender; 
		allowList[msg.sender] = true;
	}

	modifier isNode() { 
		require(allowList[msg.sender] == true, 'Your not have access, because you not node!');
		_; 
	}

	modifier isValidator() { 
		require(msg.sender == validator, 'Your not have access, because you not validator!'); 
		_; 
	}
	
	modifier isNotEmpty(address node) {
		require(node != address(0));
		_;
	}

	function pushBlock(string calldata _btfsCid) external isNode {
		lastPushedBlock = lastPushedBlock.add(1);
		cidChain[lastPushedBlock] = CidChainBlock({
			btfsCid: _btfsCid, 
			node: msg.sender,
			createdAt: block.timestamp
		});

		emit CidChainBlockPushed(_btfsCid, msg.sender);
	}

	function addToAllowList(address node) public isValidator isNotEmpty(node) {
		allowList[node] = true;
		emit ToAllowListAddressAdded(node, now);
	}

	function removeInAllowList(address node) public isValidator isNotEmpty(node) {
		allowList[node] = false;
		emit InAllowListAddressRemoved(node, now);
	}
}
